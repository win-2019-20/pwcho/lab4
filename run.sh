#!/bin/bash

for i in 1 2 3 4 5; do
    dir="zad${i}"
    src="${dir}.sh"
    log="${dir}.log"

    cd "$dir"
    echo "[$(date +%Y-%m-%d %H:%M:%S)]" >>"$log"
    source "$src"
    
    echo -n "running $dir... "
    main >>"$log" 2>&1 && echo "[OK]" || echo "[FAIL]"
    echo -n "cleaning up... "
    cleanup >>"$log" 2>&1 && echo "[OK]" || echo "[FAIL]"

    cd ..
done
