#!/bin/bash

main(){
    _www='/usr/local/apache2/htdocs/'

    docker volume create --name vol_zad2

    for i in 1 2 3; do
        docker build -t "my_apache:${i}" -f "docker.${i}" .
        docker run -td --name "Apache${i}" \
            -p "408${i}:80" \
            --mount "source=vol_zad2,target=${_www}" \
            "my_apache:${i}"
        wget -q "localhost:408${i}"
        [[ $(diff index.html "index.html.$i") ]] \
            && echo "Apache${i}: HTML served differs" \
            || echo "Apache${i}: HTML served correct"
    done
}

cleanup(){
    for i in 1 2 3; do
        rm -f "index.html.${i}"
        docker stop "Apache${i}"
        docker rm "Apache${i}"
        docker image rm "my_apache:${i}"
    done
    docker volume rm vol_zad2
}

if [[ ! "$SOURCED" ]] && [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    echo -n "sourcing ${BASH_SOURCE[0]}... "
    SOURCED=1 source "${BASH_SOURCE[0]}" && echo "[OK]" || echo "[FAIL]"
fi
