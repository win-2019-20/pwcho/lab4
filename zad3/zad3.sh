#!/bin/bash

main(){
    docker network create zad3_network

    for i in 1 2 3; do
        # iptable rules to allow ingress traffic from outside world
        sudo iptables -I INPUT -p tcp --dport "409${i}" -j ACCEPT
        # preparing logging directory
        mkdir "${HOME}/.ngnx${i}"
        # start container with logging directory bind-mounted as $HOME/.ngnx@i
        # and port-forwarding configured to accept ingress traffic
        docker run -itd --name "Nginx${i}" \
            --net zad3_network \
            -p "409${i}:80" \
            --mount "type=bind,source=${HOME}/.ngnx${i},target=/var/log/nginx" \
            nginx
        [[ -e "${HOME}/.ngnx${i}" ]] && echo "Nginx${i}: logdir created" \
                                     || echo "Nginx${i}: logdir creation failed"
        OLD_LC=$(wc -l "${HOME}/.ngnx${i}/access.log")
        wget -q --delete-after "localhost:409${i}"
        NEW_LC=$(wc -l "${HOME}/.ngnx${i}/access.log")
        (( ${NEW_LC%% *} == ${OLD_LC%% *} + 1 )) \
            && echo "Nginx${i}: access logging correct" \
            || echo "Nginx${i}: access logging not working"
    done
}

cleanup(){
    for i in 1 2 3; do
        docker stop "Nginx${i}"
        docker rm "Nginx${i}"
        rm -fr "${HOME}/.ngnx${i}"
        sudo iptables -D INPUT -p tcp --dport "409${i}" -j ACCEPT
    done
    docker network rm zad3_network
}

if [[ ! "$SOURCED" ]] && [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    echo -n "sourcing ${BASH_SOURCE[0]}... "
    SOURCED=1 source "${BASH_SOURCE[0]}" && echo "[OK]" || echo "[FAIL]"
fi
