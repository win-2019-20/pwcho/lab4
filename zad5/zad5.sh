#!/bin/bash

main(){
    grubfile='/etc/default/grub'
    OLD_GRUB_ENTRY=$(cat $grubfile | grep 'GRUB_CMDLINE_LINUX=')
    NEW_GRUB_ENTRY='GRUB_CMDLINE_LINUX="cgroup_enable=memory swapaccount=1"'
    [[ "$OLD_GRUB_ENTRY" ]] && \
        sudo perl -pi \
                  -e "s|^${OLD_GRUB_ENTRY}$|${NEW_GRUB_ENTRY}|" \
                  "$grubfile" || \
        sudo echo "$NEW_GRUB_ENTRY" >> "$grubfile"
    sudo update-grub
    echo "WARN##reboot needed to update GRUB" >&2

    docker run -itd --name limiter \
        -m 512MB --cpuset-cpus=0 \
        alpine:latest
}

cleanup(){
    docker stop limiter
    docker rm limiter
    sudo perl -pi \
              -e "s|^${NEW_GRUB_ENTRY}$|${OLD_GRUB_ENTRY}|" \
              "$grubfile"
    sudo update-grub
    echo "WARN##reboot needed to update GRUB" >&2
}

if [[ ! "$SOURCED" ]] && [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    echo -n "sourcing ${BASH_SOURCE[0]}... "
    SOURCED=1 source "${BASH_SOURCE[0]}" && echo "[OK]" || echo "[FAIL]"
fi
