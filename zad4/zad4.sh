#!/bin/bash

main(){
    # prepare image store
    store='/obrazy_priv'
    sudo mkdir "$store"
    
    # setup TLS/SSL certificate with empty* signature
    # *ubuntu doesn't allow empty signatures(!!!)
    mkdir certs
    dd if=/dev/urandom of=.rnd bs=4096 count=1
    openssl genrsa -out certs/domain.key 4096
    openssl req -new -x509 -rand .rnd \
        -subj /C=--/ST=--/L=--/O=--/OU=--/CN=-- \
        -key certs/domain.key -out certs/domain.crt

    # setup HTTPASSWD authentication
    mkdir auth
    docker run --entrypoint htpasswd --name tmpRegis \
        registry:latest -Bbn testuser testpassword >auth/htpasswd
    docker rm tmpRegis

    docker network create --ip-range '10.0.100.0/24' \
                          --subnet '10.0.100.0/24' lab4_network

    # iptable rules to allow ingress traffic from outside world
    sudo iptables -I INPUT -p tcp --dport 443 -j ACCEPT
    sudo iptables -I INPUT -p tcp --dport 6677 -j ACCEPT
    # registry container starts with pre-configured TLS and HTTPASSWD
    # available both on forwarded ports and via docker bridge @lab4_network
    # it mounts persistent authentication credentials and TLS certs
    # as well as image store bound to HOST:/obrazy_priv
    docker run -d --restart=always --name Regis1 \
        --net lab4_network --ip '10.0.100.2' \
        -p '6677:5000' \
        -p '443:443' \
        -e "REGISTRY_AUTH=htpasswd" \
        -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
        -e "REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd" \
        -e "REGISTRY_HTTP_ADDR=0.0.0.0:443" \
        -e "REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt" \
        -e "REGISTRY_HTTP_TLS_KEY=/certs/domain.key" \
        --mount "type=bind,source=$(pwd)/auth,target=/auth" \
        --mount "type=bind,source=$(pwd)/certs,target=/certs" \
        --mount "type=bind,source=${store},target=/var/lib/registry" \
        registry:latest

    docker login -u testuser -p testpassword localhost:443 \
        && echo "Regis1: TLS/HTTPASSWD setup succeeded" \
        || echo "Regis1: TLS/HTTPASSWD setup failed"

    OLD_LC=$(ls /obrazy_priv/docker/registry/v2/repositories | wc -l)
    docker tag alpine:latest localhost:443/alpine:latest
    docker push localhost:443/alpine:latest
    NEW_LC=$(ls /obrazy_priv/docker/registry/v2/repositories | wc -l)
    (( ${NEW_LC%% *} == ${OLD_LC%% *} + 1 )) \
        && echo "Regis1: authorized push succeeded" \
        || echo "Regis1: authorized push failed"
}

cleanup(){
    docker stop Regis1
    docker rm --volumes Regis1
    docker network rm lab4_network
    sudo iptables -D INPUT -p tcp --dport 443 -j ACCEPT
    sudo iptables -D INPUT -p tcp --dport 6677 -j ACCEPT
    rm -fr auth
    rm -fr certs
    sudo rm -fr "$store"
}

if [[ ! "$SOURCED" ]] && [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    echo -n "sourcing ${BASH_SOURCE[0]}... "
    SOURCED=1 source "${BASH_SOURCE[0]}" && echo "[OK]" || echo "[FAIL]"
fi
