#!/bin/bash

main(){
    docker volume create --name FirstVol

    ### A ###
    # first container recursively searches /etc directory
    # for files matching regex *a* BY NAME, writes to /data/list.txt
    # where /data is the mountpoint for @FirstVol permanent volume
    docker run -td --rm --name FirstUbu \
        --mount "source=FirstVol,target=/data" \
        ubuntu:latest \
        /bin/bash -c "find /etc -name '*a*' >/data/list.txt"
    # second container counts whitespace-separated entries in /data/list.txt
    # where /data is the mountpoint for @FirstVol permanent volume
    docker run -td --name SecondUbu \
        --mount "source=FirstVol,target=/data" \
        ubuntu:latest \
        /bin/bash -c 'wc -w /data/list.txt'
    echo "SecondUbu: wc -w /data/list.txt -> $(docker logs SecondUbu)"

    ### B ###
    # redis check asserts /data contains only list.txt
    # where /data is the mountpoint for @FirstVol permanent volume
    docker run -td --name RedisRob \
        --mount "source=FirstVol,target=/data" \
        redis:latest \
        /bin/bash -c '[[ $(ls /data) == "list.txt" ]] && echo true || echo false'
    echo "RedisRob: \$(ls /data)==list.txt -> $(docker logs RedisRob)"
}

cleanup(){
    docker stop RedisRob
    #docker prune -f
    docker rm RedisRob
    docker rm SecondUbu
    docker volume rm FirstVol
}

if [[ ! "$SOURCED" ]] && [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    echo -n "sourcing ${BASH_SOURCE[0]}... "
    SOURCED=1 source "${BASH_SOURCE[0]}" && echo "[OK]" || echo "[FAIL]"
fi
